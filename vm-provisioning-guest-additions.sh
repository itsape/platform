#!/bin/sh

# set -e # stop on all errors
set -x

export DEBIAN_FRONTEND="noninteractive"

echo "==> Installing VBox Guest Additions..."
cd
sudo mount -o loop VBoxGuestAdditions.iso /mnt
sudo /mnt/VBoxLinuxAdditions.run
RV=$?
sudo umount /mnt
sudo rm VBoxGuestAdditions.iso
exit $RV