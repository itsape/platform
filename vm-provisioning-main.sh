#!/bin/sh

set -e # stop on all errors
set -x

export DEBIAN_FRONTEND="noninteractive"


echo "==> Performing upgrade..."
sudo apt-get update
sudo apt-get -y upgrade


echo "==> Installing things..."
sudo apt-get install -y --no-install-recommends \
	ipset \
	autoconf \
	build-essential \
	dpkg-dev \
	libssl-dev \
	libyaml-dev \
	libreadline6-dev \
	linux-headers-$(uname -r) \
	zlib1g-dev \
	libncurses5-dev \
	libffi-dev \
	libgdbm6 \
	libgdbm-dev \
	liblzma-dev \
	libpq-dev \
	libsqlite3-dev \
	git \
	bison \
	libgdbm-dev \
	ruby \
	postgresql \
	postgresql-client \
	vim \
	zsh \
	screen \
	dnsutils \
	dkms \
	netcat \
	redis-server \
	unzip \
	psmisc \
	gawk \
	tcpdump \
	python3 \
	python3-pip \
	python3-venv \
	python3-setuptools \
	gnupg2 \
	aptitude \
	libsystemd-dev \
	msitools \
	pkg-config \
	6tunnel


echo "==> Building Ruby..."
mkdir -p /usr/local/etc \
	&& { \
		echo 'install: --no-document'; \
		echo 'update: --no-document'; \
	} >> /usr/local/etc/gemrc

RUBY_MAJOR='2.6'
RUBY_VERSION='2.6.3'
RUBY_DOWNLOAD_SHA256='11a83f85c03d3f0fc9b8a9b6cad1b2674f26c5aaa43ba858d4b0fcc2b54171e1'

curl -fSL -o ruby.tar.xz "https://cache.ruby-lang.org/pub/ruby/${RUBY_MAJOR%-rc}/ruby-$RUBY_VERSION.tar.xz"
echo "$RUBY_DOWNLOAD_SHA256 *ruby.tar.xz" | sha256sum -c -

mkdir -p /tmp/ruby-build
tar -xJf ruby.tar.xz -C /tmp/ruby-build --strip-components=1
rm ruby.tar.xz
cd /tmp/ruby-build
{ echo '#define ENABLE_PATH_CHECK 0'; echo; cat file.c; } > file.c.new && mv file.c.new file.c
autoconf
gnuArch="$(dpkg-architecture --query DEB_BUILD_GNU_TYPE)"
./configure --disable-install-doc --build="$gnuArch" --enable-shared
make -j "$(nproc)"
sudo make install

GEM_HOME=/usr/local/bundle
BUNDLE_PATH="$GEM_HOME"
BUNDLE_BIN="$GEM_HOME/bin"
BUNDLE_SILENCE_ROOT_WARNING=1
BUNDLE_APP_CONFIG="$GEM_HOME"

sudo mkdir -p "$GEM_HOME" "$BUNDLE_BIN"
sudo chmod 777 "$GEM_HOME" "$BUNDLE_BIN"
export PATH="$BUNDLE_BIN:$PATH"
echo 'export PATH="$BUNDLE_BIN:$PATH"' >> /home/ape/.bash_profile
sudo gem update --system

echo "==> Configuring Postgres..."
POSTGRES_MAJOR="$(psql --version | cut -d' ' -f3 | cut -d'.' -f1)"
sudo -u postgres psql -U postgres -d postgres -c "alter user postgres with password '1t54p3';"
sudo sed 's/^local\s*all\s*postgres\s*peer$/local\tall\tpostgres\tmd5/' -i.orig "/etc/postgresql/$POSTGRES_MAJOR/main/pg_hba.conf"
sudo sed -r 's/max_connections = 100/max_connections = 500/' -i.orig "/etc/postgresql/$POSTGRES_MAJOR/main/postgresql.conf"
sudo systemctl restart postgresql.service
PGPASSWORD=1t54p3 psql -U postgres -c "create user its with password 'ape';"
PGPASSWORD=1t54p3 createdb -U postgres -e -E UTF8 -O its itsape


echo "==> Installing Docker..."
sudo apt-get install -y --no-install-recommends ca-certificates curl gnupg lsb-release
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y --no-install-recommends docker-ce
sudo usermod -a -G docker ape
sudo mkdir -p /etc/docker
echo '{"ipv6":true,"fixed-cidr-v6":"fd7c:790a:4907:2::/64","experimental":true,"ip6tables":true}' | sudo tee /etc/docker/daemon.json


echo "==> Use legacy IPtables instead of nftables..."
sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy


echo "==> Configuring IPtables forward..."
sudo bash -c "echo 'net.ipv4.ip_forward = 1' >> /etc/sysctl.conf"
sudo bash -c "echo 'net.ipv4.conf.all.route_localnet = 1' >> /etc/sysctl.conf"
sudo bash -c "echo 'net.ipv4.conf.default.route_localnet = 1' >> /etc/sysctl.conf"
sudo bash -c "echo 'net.ipv4.conf.docker0.route_localnet = 1' >> /etc/sysctl.conf"
sudo bash -c "echo 'net.ipv4.conf.enp0s3.route_localnet = 1' >> /etc/sysctl.conf"
sudo bash -c "echo 'net.ipv4.conf.lo.route_localnet = 1' >> /etc/sysctl.conf"
sudo bash -c "echo 'net.ipv6.conf.all.use_tempaddr = 0' >> /etc/sysctl.conf"
sudo bash -c "echo 'net.ipv6.conf.all.forwarding = 1' >> /etc/sysctl.conf"
sudo sysctl -p

echo "==> Disable System Redis..."
sudo systemctl disable redis
sudo systemctl disable redis-server
sudo systemctl stop redis-server


echo "==> Install gitlab-runner"
# https://docs.gitlab.com/runner/executors/virtualbox.html#create-a-new-base-virtual-machine
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
sudo dpkg -i gitlab-runner_amd64.deb
rm gitlab-runner_amd64.deb


echo "==> Cleanup..."
sudo apt-get purge -y --auto-remove
sudo apt-get clean


echo "==> Installing Python Packages ..."
sudo pip3 install setuptools --upgrade
sudo pip3 install wheel
sudo pip3 install olefile


echo "==> Installing a working DHCP client"
sudo apt-get remove -y isc-dhcp-client
sudo apt-get install --no-install-recommends -y udhcpc


echo "==> Removing bash logout ..."
bash -c "echo 'exit 0' > ~/.bash_logout"


echo "==> Writing ssh key ..."
mkdir -p /home/ape/.ssh
touch /home/ape/.ssh/authorized_keys
bash -c "echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC7RhM7ZE+kMxDTb7d7nQfUdbTNBEzll1npA2GJIDcZT6vvvb5uB3RRQ/osOovh3N5J6xDB55IdFSkSebvvdxQuqOPkqPjBCTV/T9TTvre8VSCaIu5ncqiU2ocGcDZS5t0L40sRBH9dbqAMmH+b9/+cTphMmrGwkM2Zw1muxfA3VwjDFRLT8TZhhuaFsrBFKHXoN1LALYBU5AW71ESEQgN0JituLbdefSntEpU9Qvny6VBwitkh0wfusRUy5riqBDAuS3F1DVuaAFXCv6S1zdXy0q56qRX0uQoBlWiH8EeDzAHW4slj2djV+gTdwRap5RVwxvwIvhU6itgxn558PgTr root@joko' >> /home/ape/.ssh/authorized_keys"

echo "==> Creating build directories ..."
mkdir -p /home/ape/builds/itsape/artifactomat
mkdir -p /home/ape/builds/itsape/iptables_wrapper
