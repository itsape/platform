FROM ruby:2.6-bullseye

# skip installing gem documentation
RUN mkdir -p /usr/local/etc \
	&& { \
		echo 'install: --no-document'; \
		echo 'update: --no-document'; \
	} >> /usr/local/etc/gemrc \
	&& apt-get update \
  && apt-get --yes upgrade \
  && apt-get --yes install unzip libpq-dev libsqlite3-dev liblzma-dev curl python3 python3-pip python3-venv \
	&& rm -rf /var/lib/apt/lists/* \
	&& /usr/local/bin/gem update --system
