PLATFORM
========
This repository contains the platform configuration targeted by the software components of this namespace.
It does build the virtual machine and the docker container.


## REQUIREMENTS
VirtualBox is the only executor for a gitlab runner that supports virtual machines in a affordable manner.
To build a virtual machine and and make the runners of the other projects use them, they are build here and then inserted to the virtualbox library of the root user.
For this to happen, the `gitlab-runner` user needs access to `/root/VirtualBox VMs/` and the `vboxmanage` command.
This solved by allowing to run two commands by `sudo` without a password, `vboxmanage` directly and the script `/etc/gitlab-runner/scrub-vm-residuals.sh`:

```
gitlab-runner ALL=(ALL) !ALL
gitlab-runner ALL=(root) NOPASSWD: /etc/gitlab-runner/scrub-vm-residuals.sh
gitlab-runner ALL=(root) NOPASSWD: /usr/bin/vboxmanage,/usr/bin/VBoxManage
```

`scrub-vm-residuals.sh` reads:

```sh
#!/bin/bash
rm -rf /root/VirtualBox\ VMs/$1*
```
