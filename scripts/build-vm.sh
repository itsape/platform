#!/bin/bash

# ./build.sh ask|abort token

set -ex

PACKER_V='1.7.0'

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR
cd ..
pwd
wget https://releases.hashicorp.com/packer/${PACKER_V}/packer_${PACKER_V}_linux_amd64.zip
unzip -o packer_${PACKER_V}_linux_amd64.zip
rm packer_${PACKER_V}_linux_amd64.zip

# update json file with debian netinst image"
SUMS=`curl -s https://cdimage.debian.org/cdimage/archive/11.3.0/amd64/iso-cd/SHA512SUMS | grep -E 'debian-[0-9]*.[0-9]*.[0-9]*-amd64-netinst.iso'`
SHA512=`echo $SUMS | awk '{print $1}'`
ISO=`echo $SUMS | awk '{print $2}'`
echo "ISO: $ISO"

ISO_PATH="https://cdimage.debian.org/cdimage/archive/11.3.0/amd64/iso-cd/$ISO"

TEMPFILE=`mktemp`

echo "Replace iso placeholder to use $ISO_PATH"
sed -e "s|__DEBIAN_ISO_PATH__|$ISO_PATH|g" itsape-vm.template.json > $TEMPFILE
sed -e "s|__DEBIAN_ISO_SHA512__|$SHA512|g" $TEMPFILE > itsape-vm.json

rm -rf output-virtualbox-iso
./packer build -on-error="$1" -var "headless=$2" itsape-vm.json
rm -rf packer
rm -rf packer_cache
