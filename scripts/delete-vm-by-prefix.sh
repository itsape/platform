#!/bin/bash

set -x

sudo vboxmanage list vms | grep "$1" | sed -r 's/.*\{(.*)\}/\1/' | xargs -L1 -I {} sudo vboxmanage controlvm {} poweroff
sudo vboxmanage list vms | grep "$1" | sed -r 's/.*\{(.*)\}/\1/' | xargs -L1 -I {} sudo vboxmanage unregistervm {} --delete
